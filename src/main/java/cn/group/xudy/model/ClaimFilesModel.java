package cn.group.xudy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Joe on 2017/7/11.
 */
public class ClaimFilesModel extends BaseFileModel {

    @SerializedName(value="claimId", alternate="claim_id")
    public Integer claimId;

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }
}
