package cn.group.xudy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sharp on 2017/7/12.
 */
public class BaseFileModel extends BaseModel {

    @SerializedName(value="fileName", alternate="file_name")
    String fileName;

    String fileBody;
    @SerializedName(value="fileKey", alternate="file_key")
    String fileKey;

    String bucket;

    String osskey;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileBody() {
        return fileBody;
    }

    public void setFileBody(String fileBody) {
        this.fileBody = fileBody;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getOsskey() {
        return osskey;
    }

    public void setOsskey(String osskey) {
        this.osskey = osskey;
    }
}
