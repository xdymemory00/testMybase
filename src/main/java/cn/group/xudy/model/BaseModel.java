package cn.group.xudy.model;

/**
 * Created by Joe on 2017/7/5.
 */
public class BaseModel {

    Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
