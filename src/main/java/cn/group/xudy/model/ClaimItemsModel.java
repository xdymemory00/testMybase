package cn.group.xudy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Joe on 2017/7/6.
 */
public class ClaimItemsModel extends BaseModel {
    @SerializedName(value="claimContent", alternate="claim_content")
    String claimContent;
    @SerializedName(value="claimType", alternate="claim_type")
    Short claimType;
    @SerializedName(value="disputeFee", alternate="dispute_fee")
    Double disputeFee;
    @SerializedName(value="payItem", alternate="pay_item")
    Integer payItem;
    String litigants;
    @SerializedName(value="claimId", alternate="claim_id")
    Integer claimId;

    public String getClaimContent() {
        return claimContent;
    }

    public void setClaimContent(String claimContent) {
        this.claimContent = claimContent;
    }

    public Short getClaimType() {
        return claimType;
    }

    public void setClaimType(Short claimType) {
        this.claimType = claimType;
    }

    public Double getDisputeFee() {
        return disputeFee;
    }

    public void setDisputeFee(Double disputeFee) {
        this.disputeFee = disputeFee;
    }

    public Integer getPayItem() {
        return payItem;
    }

    public void setPayItem(Integer payItem) {
        this.payItem = payItem;
    }

    public String getLitigants() {
        return litigants;
    }

    public void setLitigants(String litigants) {
        this.litigants = litigants;
    }

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }
}
