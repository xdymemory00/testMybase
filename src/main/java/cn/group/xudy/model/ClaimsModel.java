package cn.group.xudy.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import javax.validation.constraints.Null;
import java.util.List;

/**
 * Created by Joe on 2017/7/6.
 */
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClaimsModel extends BaseModel {
    String reason;
    String files;
    @SerializedName(value="arbCaseId", alternate="arb_case_id")
    Integer arbCaseId;

    List<ClaimFilesModel> claimFilesModelList;

    public List<ClaimFilesModel> getClaimFilesModelList() {
        return claimFilesModelList;
    }

    public void setClaimFilesModelList(List<ClaimFilesModel> claimFilesModelList) {
        this.claimFilesModelList = claimFilesModelList;
    }

    @Override
    public String toString() {
        return "ClaimsModel{" +
                "reason='" + reason + '\'' +
                ", files='" + files + '\'' +
                ", arbCaseId=" + arbCaseId +
                '}';
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    public Integer getArbCaseId() {
        return arbCaseId;
    }

    public void setArbCaseId(Integer arbCaseId) {
        this.arbCaseId = arbCaseId;
    }
}
