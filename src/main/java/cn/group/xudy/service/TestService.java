package cn.group.xudy.service;

import cn.group.xudy.model.ClaimsModel;

import java.util.List;
import java.util.Map;

/**
 * Created by Ulegal on 2017/9/28.
 */
public interface TestService {

    List<Map<String, Object>> getListInfo();

    List<ClaimsModel> getSelectById();



}
