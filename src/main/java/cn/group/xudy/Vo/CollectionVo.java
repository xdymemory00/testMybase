package cn.group.xudy.Vo;

import cn.group.xudy.model.ClaimFilesModel;
import cn.group.xudy.model.ClaimItemsModel;
import cn.group.xudy.model.ClaimsModel;

/**
 * Created by Ulegal on 2017/9/28.
 */
public class CollectionVo {

    ClaimsModel claimsModel;

    ClaimFilesModel claimFilesModel;

//    ClaimItemsModel claimItemsModel;

    public ClaimsModel getClaimsModel() {
        return claimsModel;
    }

    public void setClaimsModel(ClaimsModel claimsModel) {
        this.claimsModel = claimsModel;
    }

    public ClaimFilesModel getClaimFilesModel() {
        return claimFilesModel;
    }

    public void setClaimFilesModel(ClaimFilesModel claimFilesModel) {
        this.claimFilesModel = claimFilesModel;
    }

//    public ClaimItemsModel getClaimItemsModel() {
//        return claimItemsModel;
//    }
//
//    public void setClaimItemsModel(ClaimItemsModel claimItemsModel) {
//        this.claimItemsModel = claimItemsModel;
//    }





}
